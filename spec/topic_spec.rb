require 'rails_helper'

describe Topic do
  let(:topic) { Fabricate(:topic) }

  describe '#seems_like_event?' do
    subject { topic.send(:seems_like_event?) }

    context 'when has one date' do
      before { topic.title = "21.12.2017 [Krakow] One, Two, Three" }
      it { should be true }
    end

    context 'when has one date without a year' do
      before { topic.title = "21.12 [Krakow] One, Two, Three" }
      it { should be true }
    end

    context 'when has date range' do
      before { topic.title = "21.12.2017-23.12.2017 [Krakow] One, Two, Three" }
      it { should be true }
    end

    context 'when location has non-standard characters' do
      before { topic.title = "21.12.2017 [Bielsko-Biała, Polska] One, Two, Three" }
      it { should be true }
    end
  end

  describe 'when title has a one-day event format' do
    before { topic.update_attribute(:title, '21.12.2017 [Krakow] One, two, three') }

    it 'should set cusom_fields for single day event' do
      expect(topic.custom_fields.keys).to include 'event_beginning_at'
      expect(topic.custom_fields.keys).to include 'event_end_at'
      expect(topic.custom_fields.keys).to include 'event_location'
      expect(topic.custom_fields.keys).to include 'event_enabled'

      expect(topic.custom_fields['event_beginning_at']).to eq '2017-12-21'
      expect(topic.custom_fields['event_location']).to eq 'Krakow'
      expect(topic.custom_fields['event_enabled']).to eq 't'
    end

    context 'when date is invalid' do
      before { topic.update_attribute(:title, '21.21.2017 [Krakow] One') }

      it 'does not save custom_fields for event' do
        expect(topic.custom_fields['event_beginning_at']).to eq nil
        expect(topic.custom_fields['event_location']).to eq nil
	expect(topic.custom_fields['event_enabled']).to eq 'f'
      end
    end

    context 'when single date does not have a year in it' do
      before { topic.update_attribute(:title, '21.12 [Krakow] One') }

      it 'sets a year as topic creation year' do
	year = topic.created_at.year
        expect(topic.custom_fields['event_beginning_at']).to eq "#{year}-12-21"
      end
    end
  end

  describe 'when title has multi-day event format' do
    before { topic.update_attribute(:title, '12.03.2017-14.03.2017 [Krakow] One') }
    
    it 'sets proper beginning_at and end_at fields' do
      expect(topic.custom_fields['event_beginning_at']).to eq '2017-03-12'
      expect(topic.custom_fields['event_end_at']).to eq '2017-03-14'
    end

    context 'when end date has no year set' do
      context 'when end_at with year taken from beginning_at is after the beginning_at' do
        before { topic.update_attribute(:title, '12.12-14.12 [Krakow] One') }

        it 'sets end_date year to beginning_at year' do
	  topic_year = topic.created_at.year
          expect(topic.custom_fields['event_end_at']).to eq "#{topic_year}-12-14"
	end
      end

      context 'when end date with year taken from beginning date is before the beginning date' do
        before { topic.update_attribute(:title, '12.12-02.01 [Krakow] One') }

        it 'sets end_date year to the next one after beginning_date' do
	  topic_year = topic.created_at.year
          expect(topic.custom_fields['event_end_at']).to eq "#{topic_year + 1}-01-02"
	end
      end
    end
  end
end
