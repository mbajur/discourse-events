# name: hcpl-events
# about: hcpl events plugin
# version: 0.0.1.1
# authors: m.b.

after_initialize do
  Topic.class_eval do

    EVENT_TOPIC_FORMAT = /^([0-9.-]+)\[([[:alpha:]\-\,\.\ ]+)\](.+)/.freeze
	  
    before_save :resolve_event_topic, if: :seems_like_event?

    def resolve_event_topic
      logger.debug "Treating topic as an event topic."

      begin
        # Parse title segments
        parsed_title = EVENT_TOPIC_FORMAT.match(title.delete(' '))

        # Extract dates
        dates = parsed_title[1].split('-')
        beginning_at = dates[0]

        beginning_at_parts = beginning_at.split('.').map(&:to_i)
        beginning_at_day   = beginning_at_parts[0]
        beginning_at_month = beginning_at_parts[1]
        beginning_at_year  = beginning_at_parts[2] || created_at.year

        beginning_at = Date.new(
          beginning_at_year, beginning_at_month, beginning_at_day
        )
        logger.debug "Beginning at set to #{beginning_at}"

        # Setting end_at
        if dates.length > 1
          end_at = dates[1]

          end_at_parts = end_at.split('.').map(&:to_i)
          end_at_day   = end_at_parts[0]
          end_at_month = end_at_parts[1]
          end_at_year  = end_at_parts[2] || created_at.year

          end_at = Date.new(
	    end_at_year, end_at_month, end_at_day
          )

	  # Make sure end_at is bigger than beginning_at
	  end_at = end_at.change(year: beginning_at_year + 1) if beginning_at > end_at

          logger.debug "End at set to #{end_at}"
        end

        # Extract location
        location = /\[([[:alpha:]\-\.\,\ ]+)\]/.match(title)[1]
        logger.debug "Location set to #{location}"

        # Persist event fields
        self.custom_fields = {
	  event_enabled:      true,
	  event_beginning_at: beginning_at.to_s,
	  event_end_at:       end_at.to_s,
	  event_location:     location
        }

      rescue => e
        logger.warn "Something went wrong with event data extraction: #{e.message}"
	self.custom_fields = {
	  event_enabled:      false,
          event_beginning_at: nil,
	  event_end_at:       nil,
	  event_location:     nil
	}
      end
    end

    private

    def seems_like_event?
      tit = title
      tit = tit.delete(" ")

      result = (tit =~ EVENT_TOPIC_FORMAT)
      result != nil
    end
  end

  module ::DiscourseEvents
    class Engine < ::Rails::Engine
      engine_name "discourse-events"
      isolate_namespace DiscourseEvents
    end
  end

  DiscourseEvents::Engine.routes.draw do
    get '/events' => 'events#index'
  end

  Discourse::Application.routes.append do
    mount ::DiscourseEvents::Engine, at: '/callendar'
  end

  require_dependency 'list_controller'

  class DiscourseEvents::EventsController < ListController
    def index
      topics = TopicCustomField.where(name: 'event_enabled', value: 't').includes(:topic, topic: :_custom_fields) 

      topics.map do |t|
        { id: t.topic.id, custom_fields: t.topic.custom_fields}
      end

      render json: topics
    end
  end
end
